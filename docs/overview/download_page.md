# Downloads

## Binaries

Download the last binary files for:

- Windows
    - Install the last [Visual C++ redistributable packages for Visual Studio 2019](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0)
    - <a href="../../assets/bin/COMFOR_V0_5_1.exe" download>Click to Download Comfor V0.5.1 for Windows</a>
- Ubuntu 
    - <a href="../../assets/bin/COMFOR_V0_5_0_GCC_10_2_0" download>Click to Download Comfor V0.5.0 for Linux (GCC 10.2.0)</a>
- macOS
    - <a href="../../assets/bin/COMFOR_V0_5_1_OSX_clang11" download>Click to Download Comfor V0.5.1 for macOS (Clang 11.0.3)</a>

## Examples 

Some examples to run with comfor:

- <a href="../../assets/examples/examples.zip" download>Click to Download the examples</a>
